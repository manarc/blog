---
title: "Helm"
date: 2024-01-03T17:15:48+09:00
authors: ["chojeonghak"]
#cover:
#  image: 
#  caption: kubernetes
#  style: Full;
categories:
  - helm
tags:
  - helm
  - install
slug: helm

toc: false
draft: false
---
# 헬름 개요
헬름을 사용하여 쿠브네티스 애플리케이션의 생성, 구성, 배포, 패키징을 자동화한다.

**차트**는 쿠버네티스 클러스터로 애플리케이션을 배포하기위해 필요한 리소스의 묶음이다.
**저장소**를 통해 쿠버네티스 리소스 파일의 묶음을 가져올 수 있다.
**릴리즈**는 차트를 통해 자신의 쿠버네티스 클러스터에 배포되어 구동중인 애플리케이션 인스턴스이다. 
## 설치
우분투에서 헬름은 아래 스크립트로 설치할 수 있다. [참고사이트](https://helm.sh/docs/intro/install/)
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
## 사용법
> 헬름이 어렵게 느껴지는 이유는 쿠버네티스의 수많은 속성이 속성파일인 values.yml에 모두 노출이 되기 때문이다. 난이도가 있는 부분은 쿠버네티스의 개념이고 헬름은 쿠버네티스를 적은 노력으로 쉽게 관리하도록 돕는 사용법이 쉬운 도구이다. 

도구 사용에 대한 이해를 돕기위해 가장 간단한 형태의 애플리케이션을 배포하는 방법에 대해 설명한다. 

먼저 차트를 하나 생성한다.
```
alias h=helm
h create mypod
```
생성된 mypod 폴더로 가서 values.yaml 을 열면 쿠버네티스의 리소스인 인그레스, 디플로이먼트, 서비스, 볼륨 등과 관련한 수많은 속성들이 나열되어 있다. 

헬름의 기본 사상은 이 속성의 제어 만으로 정형화되고 분산되어 있는 쿠버네티스의 매니페스터 파일 수정을 한번에 처리하자는 것이다.

자동 생성되는 폴더 구조는 다음과 같다. 

[상세참고](https://helm.sh/docs/topics/charts/)
``` 
mypod/
  Chart.yaml             # 차트 정보  
  values.yaml            # 차트에서 사용하는 기본 값
  .helmignore            # 패키징 시 제외 대상
  charts/                # 의존 차트
  templates/             # 템플릿
  templates/NOTES.txt    # 간략 사용법 기술
  templates/_helpers.tpl #
```
템플릿 폴더에 deployment, service, service account, ingress, hpa ayml  파일이 위치되어 있다. 지금 필요한 건 pod 생성 용 yaml이니 모든 yaml 파일을 지우고 아래의 pod.yml 파일을 생성한다.
```
apiVersion: v1
kind: Pod
metadata:
  name: {{ include "mypod.fullname" . }}
spec:
  containers:
    - name: {{ .Chart.Name }}
      image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
      ports:
        - containerPort: {{ .Values.service.port }}
          protocol: TCP
```
템플릿 파일에 보면 위의 yaml을 렌더링하는 문법이 들어가있다.
``` Go
{{/*
Expand the name of the chart.
*/}}
{{- define "mypod.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
```
values.yaml에는 대체될 문자열이 들어가 있다. 

렌더링 된 모습을 확인하기 위해 다음의 명령을 실행한다.
``` shell
h template mypod
```
```
---
# Source: mypod/templates/pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: mypod
      image: "nginx:1.16.0"
      ports:
        - containerPort: 80
          protocol: TCP
```
문법 검사
```
h lint mypod
```
실행전 모의 테스트
```
helm install --dry-run myapp ./mypod
```
실행
```
helm install --dry-run myapp ./mypod
```
쿠버네티스 실행 확인
```
alias k=kubectl
k get po
NAME                        READY   STATUS       RESTARTS   AGE
mypod                   1/1     Running      0          20s

```
삭제
```
h delete mypod
release "mypod" uninstalled
```



# 참고
+ [설치, 공식 사이트](https://helm.sh/ko/docs/intro/install/)