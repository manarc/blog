---
title: "istio-benefit"
date: 2024-01-04T18:15:48+09:00
authors: ["chojeonghak"]
#cover:
#  image: 
#  caption: kubernetes
#  style: Full;
categories:
  - istio
tags:
  - istio
  - kubernetes
slug: istio-benefit

toc: false
draft: false
---
## 서비스 디스커버리
 컨테이너화된 앱은 lifetime 동안 여러 번 시작되고 중지될 수 있다. 그럴 때마다, 앱에 새로운 주소가 부여될 수 있으므로 이를 찾으려는 다른 앱에게는 새로운 위치 정보를 제공하는 도구가 필요하다. 서비스 디스커버리는 네트워크 내의 앱에 대한 추적을 지속하여 필요시 서로를 찾을 수 있도록 한다. 서비스들에 대한 공통된 공간을 제공하여, 개별 서비스들이 잠재적으로 식별 및 검색될 수 있도록 한다. 

 ## 쿠버네티스 서비스 디스커버리
 ### 서버 사이드 서비스 디스커버리
 쿠버네티스 서비스 오브젝트를 생성하면 로드밸런서가 매핑된다. 서비스 검색을 로드 밸런서를 통해 진행한다. 
 ### 클라이언트 사이드 서비스 디스커버리
 서비스 디스커버리 로직을 유레카 같은 클라이언트 애플리케이션에 둔다. 서비스 레지스터리를 이용해 가용 인스턴스의 주소를 등록하고 검색한다. 단일 장애 지점이 될 수 있는 로드 밸런서보다 병목 상태 발생 확율이 낮다. 단점은 레지스터리와 클라이언트들을 커플링해야 하고 `복잡도가 증가`하는 점이 있다.
 ``` java
 DiscoveryClient discoveryClient = new DiscoveryClient(applicationName, eurekaClientConfig);
List<ServiceInstance> instances = discoveryClient.getInstances(serviceName);

// Select a service instance based on load balancing algorithm
ServiceInstance selectedInstance = loadBalancer.choose(instances);

// Send request to the selected service instance
String targetUrl = selectedInstance.getUri().toString();
HttpResponse response = httpClient.sendRequest(targetUrl, request);
 ```
 ### DNS 서비스 디스커버리
 쿠버네티스에서 기본적으로 활성화된 기능으로 DNS와 서비스 명을 매핑한다. 빌트인 기능이므로 간단하고 효율적으로 서비스 디스커버리 기능을 사용할 수 있다. DNS 서비스는 커스터 마이징 가능하며 CLI를 통한 설정 파일 변경이 필요하다.
``` java
import requests

response = requests.get("http://my-service.my-namespace.svc.cluster.local")
print(response.text)
```

## BEST Practise ( 서비스 메시 활용)
- 쿠버네티스 오브젝트에 `라벨과 SELECTOR`를 반드시 추가하여 서비스 디스커버리의 효율성을 높인다.
- DNS는 기본적으로 지원되는 디스커버리 메커니즘이다. IP대신 DNS형태의 서비스 명으로 접근하므로 확장 가능한 시스템을 간단히 구축할 수 있다.
- 트래픽 관리를 위한 인프라 레이어를 제공하는 서비스 메시를 활용한다. 
## 라우팅
- 로드 밸런싱
- 헬스 체크
- 자동 배포 - 가중치에 따라 트래픽 새 노드로 유드
## 복구
- 서비스 타임아웃
- 재시도 회수
## 보안
- 키 관리
- TLS 기반 암호화
## 텔레메트리
- 네트워크 호출의 흐름 추적
- 메트릭 수집

# 참고
[Basic Guide to Kubernetes Service Discovery](https://dev.to/nomzykush/basic-guide-to-kubernetes-service-discovery-dmd)