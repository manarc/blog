---
title: "JWT"
date: 2021-09-02T09:18:45+09:00
draft: true
---

# JWT

## JSON Web Token

API 인증은 모든 클라이언트에 대한 고유의 해시값을 저장소에 저장하고,  매번의 요청에 대해 해시값을 확인하는 과정입니다. 저장소로 데이터베이스를 사용할 경우 시스템의 성능에 지장을 줄 수 있습니다. 

JWT는 클라이언트의 크레덴셜을 확인하기 위해 HTTP 요청으로 전송되는 문자열입니다.  JWT는 데이터베이스에 저장할 필요가 없으며, 클라이언트가 서버로 부터 받은 JWT를 저장한 후 이 후 전송되는 모든 API에 적용하게 됩니다. 

## Secret Key

JWT 생성시 참조되는 참조 키로 서버에 저장되어 클라이언트에게 노출이 되지 않습니다. 클라이언트는 서버의 키를 모르기 때문에 클라이언트가 빌드 시 생성되는 JWT 결과 값이 달라집니다. 참조 키는 일반 문자열입니다.

## JWT 구조

JWT는 도트(.)로 이어진 헤더, 페이로드, 시그너처 문자열이 base64로 인코딩 된 값입니다.

```
eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwaG9lbml4IiwiZXhwIjoxNjMxMjUzMjkxLCJpYXQiOjE2MzExNjY4OTF9.3kwGqq6e_ks1__T1X4MVqys-OoPaavbCzxvwgwm9CQY
```

헤더는 토큰 유형과 암호화 알고리즘 정보를 포함합니다. 

```
{
  "alg": "HS256",
  "typ": "JWT"
}
```

페이로드는 토큰의 내용으로 데이터 크기를 적게 유지 할수록 좋습니다. base 64로 인코딩되어 누구나 확인이 가능하므로 패스워드와 같은 민감한 정보를 기입하지 않도록 합니다. 클레임의 집합이며, 클레임은 엔티티를 포함합니다. 공개 클레임, 비공개 클레임, 시스템에서 예약한 iss (issuer), exp (expiration time), sub (subject), aud (audience) 클레임 이 있습니다. sub 클레임은 JWT의 고유 식별 값입니다.  

```
{
  "sub": "1234567890",
  "name": "John Doe",
  "admin": true
}
```

시그너처에는 Secret Key로 암호화된 값이 들어가 있어 공개되지 않는 영역입니다.



## 토큰 생성기

다음은 자바로 JWT 를 생성하는 예시입니다.

먼저 참조 라이브러리를 등록합니다.

```
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.1</version>
		</dependency>
```

Jwts 클래스를 사용하여 HashMap으로 클레임을 구성한 후 bullder()에 클레임, Subject 및 생성 일 , 만료일을 입력하여 토큰을 생성합니다.

```
	public String generateToken(String username) {
		Map<String, Object> claims = new HashMap<>();
		return createToken(claims, username);
	}
	
	private String createToken(Map<String, Object> claims, String subject) {
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subject)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRE_MIN))
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}
```



### 토큰에서 정보 추출

클라이언트가 전달한 토큰 내 정보를 추출하는 예시입니다.

```
	public String extractUsername(String token) {
		return extractClaim(token, Claims::getSubject);
	}

	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	private Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}
```



### 토큰 추출

클라이언트로 부터 요청받은 API 요청이 유효한 토큰인지 확인하는 과정입니다. HTTP 헤더를 분석하여 Authorization 헤더의 키 값은 Bearer {토큰내용} 형태로 구성되어 있습니다.  토큰을 헤더로 부터 추출하여 유효성 검사를 진행합니다.

```
@Component
public class JwtFilter extends OncePerRequestFilter {
	@Autowired
	private TokenUtil jwtUtil;
	
	@Autowired
	private UserDetailsService service;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			FilterChain filterChain) throws ServletException, IOException {
		String authorizationHeader = httpServletRequest.getHeader("Authorization");
		String token = null;
		String userName = null;
		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			token = authorizationHeader.substring(7);
			userName = jwtUtil.extractUsername(token);
		}
		if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = service.loadUserByUsername(userName);
			if (jwtUtil.validateToken(token, userDetails.getUsername())) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}
}

```

